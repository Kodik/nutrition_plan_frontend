import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faDragon } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faDragon)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'https://nutrition-plan-project.herokuapp.com'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
axios.defaults.headers.get['Content-Type'] = 'application/json'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
